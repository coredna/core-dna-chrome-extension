
var vcs, //Store ?vcs=1
	queryStringModifier; //check to see if I need an amp or ?

if (window.location.search) {
	queryStringModifier = '&';
} else {
	queryStringModifier = '?';
}

jQuery('body').addClass('debug__window--open');

if (jQuery('.debug__window').length == 0) {
	jQuery('body').append('<div class="debug__window loading"><div class="debug__logo" /></div></div>');
	jQuery('.debug__window').append('<div class="debug__dev"><a href="'+window.location+queryStringModifier+'dev=1&err=1">Open dev=1</a><a href="'+window.location+queryStringModifier+'dev=2">Open dev=2</a><a href="'+window.location+queryStringModifier+'cdn=1">Open cdn=1</a><input type="checkbox" id="cdnCheck" name="cdnCheck" class="debug__input--cdn" /><label for="cdnCheck" class="debug__label--cdn">Toggle cdn checker</label></div><div class="debug__vcs"><div class="loader"><div class="spinner"></div></div><pre></pre></div><a href="#" class="debug__close">Close</a>');
}


jQuery.ajax({
	url: window.location+queryStringModifier+'vcs=1',
	type: 'GET',
	dataType: 'html'
}).done(function(data) {
	vcs = jQuery(data).html();
	jQuery('.debug__vcs pre').html(vcs);
	jQuery('.debug__window').removeClass('loading');

});

jQuery('.debug__window .debug__close').click(function() {
	jQuery('.debug__window pre').html('');
	jQuery('body').removeClass('debug__window--open');
	return false;
});

jQuery('#cdnCheck, label[for="cdnCheck"]').click(function() {
	if (jQuery('#cdnCheck:checkbox:checked').length > 0) {
		jQuery('body').addClass('debug__cdn');
		jQuery('link[hrefjQuery=".css"]:not([href*="corednacdn"])').each(function() {
			if (jQuery('link[hrefjQuery=".css"]:not([href*="corednacdn"])').length) {
				var cssSrc = jQuery(this).attr('href')
				jQuery('body').prepend('<div class="debug__css">'+cssSrc+'</div>');
			}
		});
	} else {
		jQuery('body').removeClass('debug__cdn');
		jQuery('.debug__css').remove();
	}
});


