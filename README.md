# README v0.0.1 #

Expose some handy info for Core Dna sites

### What is this repository for? ###

* Hosting of the plugin

### How do I get set up? ###

* Clone the repo, click and drag the CoreDna folder into your extensions tab ( go to [chrome://extensions/](Link URL) in your URL bar )

* If you are altering the scss use sass coredna.scss:coredna.css --style compressed --sourcemap=none to recompile

### Contribution ###

* Feel free to fork and add whatever you find useful!